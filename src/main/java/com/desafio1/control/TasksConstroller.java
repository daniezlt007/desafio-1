package com.desafio1.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.desafio1.model.Tasks;
import com.desafio1.service.TasksService;

@Controller
@RequestMapping("/tasks")
public class TasksConstroller {

	@Autowired
	private TasksService tasksService;

	public TasksConstroller(TasksService tasksService) {
		super();
		this.tasksService = tasksService;
	}

	@GetMapping("/novo")
	public String create(Model model) {
		model.addAttribute("tasks", new Tasks());
		return "pages/create";
	}

	@GetMapping("/index")
	public String mostrarTarefas(Model model) {
		Iterable<Tasks> lista = this.tasksService.listTask();
		model.addAttribute("lista", lista);
		return "pages/tarefas";
	}

	@GetMapping("/atualizar/{id}")
	public String atualizarStatus(@ModelAttribute("id") Long id, Model model) {
		Tasks taskUpdate = this.tasksService.updateById(id);
		model.addAttribute("tasks", taskUpdate);
		return "redirect:/tasks/index";
	}

	@PostMapping("/save")
	public String salvar(@ModelAttribute("tasks") Tasks tasks, Model model) {
		this.tasksService.createTasks(tasks);
		return "redirect:/tasks/index";
	}
	
	@PostMapping("/deletar")
	public String deletarLista() {
		this.tasksService.deletar();
		return "redirect:/tasks/index";
	}

}
