package com.desafio1.repositorys;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.desafio1.model.Tasks;

@Repository
public interface TaskRepository extends CrudRepository<Tasks, Long>{
	
	@Query("SELECT t FROM Tasks t WHERE t.status=false order by t.id")
	public Iterable<Tasks> findAllStatus(); 
	
}
