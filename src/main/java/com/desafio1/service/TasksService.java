package com.desafio1.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio1.model.Tasks;
import com.desafio1.repositorys.TaskRepository;

@Service
public class TasksService implements TasksImplementation {
	
	@Autowired
	private TaskRepository taskR;
	
	public TasksService(TaskRepository taskR) {
		super();
		this.taskR = taskR;
	}
	
	@Override
	public Tasks createTasks(Tasks tasks) {
		return this.taskR.save(tasks);
	}

	@Override
	public void deletar(Long id) {
		Optional<Tasks> taskDeleted = this.taskR.findById(id);
		if(taskDeleted != null) {
			this.taskR.deleteById(taskDeleted.get().getId());
		}
	}

	@Override
	public Iterable<Tasks> listTask() {
		return this.taskR.findAllStatus();
	}
	
	@Override
	public Tasks findById(Long id) {
		Optional<Tasks> task = this.taskR.findById(id);
		return task.get();
	}

	@Override
	public Tasks updateById(Long id) {
		Optional<Tasks> tasks = this.taskR.findById(id);
		if(tasks != null) {
			Tasks task =  tasks.get();
			task.setStatus(true);
			this.taskR.save(task);
		}
		return null;
	}

	@Override
	public void deletar() {
		this.taskR.deleteAll();
	}

}
