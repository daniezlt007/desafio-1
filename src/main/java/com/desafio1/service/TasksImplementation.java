package com.desafio1.service;

import com.desafio1.model.Tasks;

public interface TasksImplementation {

	public Tasks createTasks(Tasks tasks);
	public void deletar(Long id);
	public Iterable<Tasks> listTask();
	public Tasks findById(Long id);
	public Tasks updateById(Long id);
	public void deletar();
	
}

